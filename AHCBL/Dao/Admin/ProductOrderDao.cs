﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class ProductOrderDao : BaseDao<ProductOrderDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<ProductOrderDto> GetDataList()
        {
            try
            {
                List<ProductOrderDto> list = new List<ProductOrderDto>();
                //dt = GetStoredProc("PD009_GET_CATEGORY");
                dt = GetStoredProc("PD023_GET_PRODUCT_ORDER");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new ProductOrderDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        code = Util.NVLString(dr["code"]),
                        username_buy = Util.NVLString(dr["username_buy"]),
                        fullname_buy = Util.NVLString(dr["fullname_buy"]),
                        username_sale = Util.NVLString(dr["username_sale"]),
                        fullname_sale = Util.NVLString(dr["fullname_sale"]),
                        product_name = Util.NVLString(dr["product_name"]),
                        price_interest =  Util.NVLString(Util.NVLDecimal(dr["price_interest"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        fee = Util.NVLString(Util.NVLDecimal(dr["fee"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        buy_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["buy_date"]).ToString("yyyyMMddHH:mm:ss"))),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
    }
}