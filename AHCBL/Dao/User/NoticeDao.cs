﻿using AHCBL.Component.Common;
using AHCBL.Dto.User;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.User
{
    public class NoticeDao : BaseDao<NoticeDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<NoticeDto> GetDataQA()
        {
            try
            {
                List<NoticeDto> list = new List<NoticeDto>();
                dt = GetStoredProc("PD054_GET_QA_WRITE");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new NoticeDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        name = Util.NVLString(dr["name"]),
                        comment = Util.NVLString(dr["comment"]),
                        status = Util.NVLString(Util.NVLInt(dr["status"])==1 ? "답변대기" : "댓글"),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMdd"))),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public string SaveData(string content)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD053_QA_WRITE", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@name", Util.NVLString(content));
                AddSQLParam(param, "@member_id", Util.NVLInt(Varible.User.member_id));

                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
        
    }
}
