﻿using AHCBL.Component.Common;
using AHCBL.Dto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao
{
    public class ChartMemberDao : BaseDao<ChartMemberDao>
    {
        private DataTable dt;
        public List<ChartMemberDto> GetDataList()
        {
            try
            {
                List<ChartMemberDto> list = new List<ChartMemberDto>();
                dt = GetStoredProc("PD073_GET_CHART_MEMBER");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new ChartMemberDto
                    {

                        id = Util.NVLInt(dr["id"]),
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        adviser = Util.NVLString(dr["adviser"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public List<object> GetChartData()
        {
            try
            {
                List<object> list = new List<object>();
                dt = GetStoredProc("PD073_GET_CHART_MEMBER");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new object[]
                    {
                        Util.NVLInt(dr["member_id"]), Util.NVLString(dr["username"]), Util.NVLString(dr["adviser_id"])
                    });

                    //    list.Add(
                    //    new ChartMemberDto
                    //    {

                    //        id = Util.NVLInt(dr["id"]),
                    //        username = Util.NVLString(dr["username"]),
                    //        fullname = Util.NVLString(dr["fullname"]),
                    //        adviser = Util.NVLString(dr["adviser"]),
                    //    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }

    }
}