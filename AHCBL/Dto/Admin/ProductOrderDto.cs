﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.Admin
{
    public class ProductOrderDto
    {
        public int id { get; set; }
        public int num { get; set; }
        public string code { get; set; }
        public string username_buy { get; set; }
        public string fullname_buy { get; set; }
        public string username_sale { get; set; }
        public string fullname_sale { get; set; }
        public string product_name { get; set; }
        public string price_interest { get; set; }
        public string fee { get; set; }
        public string create_date { get; set; }
        public string buy_date { get; set; }
        public string status { get; set; }

    }
}
