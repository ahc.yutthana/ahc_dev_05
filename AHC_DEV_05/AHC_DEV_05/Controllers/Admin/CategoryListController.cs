﻿using AHCBL.Component.Common;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class CategoryListController : Controller
    {
        Permission checkuser = new Permission();
        // GET: CategoryList
        public ActionResult Index(int? page)
        {
            checkuser.chkrights("admin");
            var data = CategoryListDao.Instance.GetDataList();
            string count = string.Empty;
            if (data.Count < 1000)
            {
                count = data.Count.ToString();
            }
            else
            {
                count = data.Count.ToString("0,0", CultureInfo.InvariantCulture);
            }
            ViewBag.Count = count;            
            int rows = Convert.ToInt32(ConfigurationManager.AppSettings["rows"]);
            ViewBag.rows = rows;
            return View(data.ToList().ToPagedList(page ?? 1, Util.NVLInt(rows)));

        }
        public ActionResult Create(int id)
        {
            var data = CategoryListDao.Instance.GetDataList().Find(smodel => smodel.id == id);
            data.code = data.code + "10";
            return View(data);
        }


        [HttpPost]
        public ActionResult Create(CategoryListDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    string result = CategoryListDao.Instance.SaveDataList(model, "add");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                        ModelState.Clear();
                    }
                    else
                    {
                        //ViewBag.Status = TempData["Dropdown"];
                        ViewBag.Message = "Successfully !!";
                        ModelState.Clear();
                    }
                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message;
                return View();
            }
        }
        public ActionResult Edit(int id)
        {
            var data = CategoryListDao.Instance.GetDataList().Find(smodel => smodel.id == id);
            return View(data);
        }
        [HttpPost]
        public ActionResult Edit(CategoryListDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string result = CategoryListDao.Instance.SaveDataList(model, "edit");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                    return View();
                }
                else
                {
                    return View();
                }
                
            }
            catch
            {
                return View();
            }
        }
        public ActionResult Delete(CategoryListDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //model.id = id;
                    CategoryListDao.Instance.SaveDataList(model, "del");
                    return Json("Delete Successfully !");
                }
                else
                {
                    return Json("Data not found !");
                }
            }
            catch (Exception e)
            {
                return Json("Error");
            }
            //}
            //try
            //{
            //    string result = PopupListDao.Instance.SaveDataList(model, "del");
            //    if (result == "OK")
            //    {
            //        ViewBag.Message = "Student Deleted Successfully";
            //    }
            //    return RedirectToAction("Index");
            //}
            //catch (Exception e)
            //{
            //    ViewBag.Message = "Error : " + e.Message.ToString();
            //    return View();
            //}
        }
    }
}