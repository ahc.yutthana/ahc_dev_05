﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class RequestListController : Controller
    {
        Permission checkuser = new Permission();
        // GET: RequestList
        public ActionResult Index(string drp, string keyword, int? page)
        {
            checkuser.chkrights("admin");
            int rows = Util.NVLInt(Varible.Config.page_rows);
            var data = RequestListDao.Instance.GetDataList();
            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            TempData["data1"] = data.ToList();
            int cnt = data.ToList().Count();
            Session["Status"] =  DropdownDao.Instance.GetStatus();
            Session["dropdown"] = DropdownDao.Instance.GetDrpPoint(1);

            int amount = GetDataDao.Instance.GetWithdrawalSumAmount(keyword);
           

          
            if (drp == "username")
            {
                TempData["data"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, rows);
                TempData["data1"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList();
                cnt = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().Count();
                //return View(data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, rows));
            }
            if (amount < 1000)
            {
                Session["total"] = amount.ToString();
            }
            else
            {
                Session["total"] = amount.ToString("0,0", CultureInfo.InvariantCulture);
            }

            if (cnt < 1000)
            {
                Session["count"] = cnt.ToString();
            }
            else
            {
                Session["count"] = cnt.ToString("0,0", CultureInfo.InvariantCulture);
            }
            ViewBag.Amount = Session["total"];
            ViewBag.Count = Session["count"];
            ViewBag.Search = Session["dropdown"];
            ViewBag.Rows = rows;
            ViewBag.Status = Session["Status"];

            Session["data"] = TempData["data1"];
            return View(TempData["data"]);
            //return View(data.ToList().ToPagedList(page ?? 1, rows));
        }
        public ActionResult Updatedata(int id,int status)
        {
            try
            {

                RequestListDto model = new RequestListDto();
                model.id = id;
                model.active = status;     
                string result = RequestListDao.Instance.SaveDataList(model);
                if (result != "OK")
                {
                    return Json(result);
                }
                else
                {
                    return Json("Successfully !");
                }

            }
            catch (Exception e)
            {
                return Json("Error !!");
            }
        }
        public ActionResult ExportToExcel()
        {
            try
            {
                var data = Session["data"] as List<RequestListDto>;
                if (data != null)
                {
                    ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    ExcelPackage Ep = new ExcelPackage();
                    ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
                    Sheet.Cells["A1"].Value = "회원아이디";
                    Sheet.Cells["B1"].Value = "이름";
                    Sheet.Cells["C1"].Value = "주문상품";
                    Sheet.Cells["D1"].Value = "개수";
                    Sheet.Cells["E1"].Value = "가격";
                    Sheet.Cells["F1"].Value = "일시";
                    Sheet.Cells["G1"].Value = "상태";
                    int row = 2;
                    foreach (var item in data)
                    {
                        Sheet.Cells[string.Format("A{0}", row)].Value = item.username.ToString();
                        Sheet.Cells[string.Format("B{0}", row)].Value = item.fullname.ToString();
                        Sheet.Cells[string.Format("C{0}", row)].Value = item.total.ToString();
                        Sheet.Cells[string.Format("D{0}", row)].Value = item.amount.ToString();
                        Sheet.Cells[string.Format("E{0}", row)].Value = item.price.ToString();
                        Sheet.Cells[string.Format("F{0}", row)].Value = item.create_date.ToString();
                        Sheet.Cells[string.Format("G{0}", row)].Value = item.active!=2 ? "완료" : "대기";
                        row++;
                    }
                    Sheet.Cells["A:AZ"].AutoFitColumns();
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment: filename=" + "Report.xlsx");
                    Response.BinaryWrite(Ep.GetAsByteArray());
                    Response.End();


                }

                ViewBag.Amount = Session["total"];
                ViewBag.Count = Session["count"];
                ViewBag.Search = Session["dropdown"];
                ViewBag.Status = Session["Status"];
                ViewBag.Rows = Util.NVLInt(Varible.Config.page_rows);

                return View("Index");
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }
    }
}