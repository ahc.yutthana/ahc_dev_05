﻿using AHCBL.Component.Common;
using AHCBL.Dao.Admin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class DashboardController : Controller
    {
        Permission checkuser = new Permission();
        // GET: Dashboard
        public ActionResult Index()
        {
            checkuser.chkrights("admin");
            ViewBag.rows = Util.NVLInt(ConfigurationManager.AppSettings["dashboard_rows"]);
            var data = DashboardDao.Instance.GetData();
            ViewBag.MemberNo = data.tab1.Count();
            ViewBag.PointNo = data.tab3.Count();

            return View(data);
        }
    }
}