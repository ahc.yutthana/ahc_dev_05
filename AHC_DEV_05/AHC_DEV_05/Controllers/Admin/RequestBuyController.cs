﻿using AHCBL.Component.Common;
using AHCBL.Dao.Admin;
using AHCBL.Dto;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class RequestBuyController : Controller
    {
        // GET: RequestBuy
        public ActionResult Index(int? page, string keyword)
        {
            var data = RequestBuyDao.Instance.GetDataList();
            int rows = Util.NVLInt(ConfigurationManager.AppSettings["rows"]);
            string count = string.Empty;
            if (data.Count < 1000)
            {
                count = data.Count.ToString();
            }
            else
            {
                count = data.Count.ToString("0,0", CultureInfo.InvariantCulture);
            }
            ViewBag.Count = count;
            ViewBag.Rows = rows;

            var Matching = RequestBuyDao.Instance.GetDataMatching(1);
            ViewBag.Matching = Matching.ToList();
            ViewBag.MatchingCnt = Matching.Count();

            return View(data.ToList().ToPagedList(page ?? 1, rows));

        }
        public ActionResult getdata(int id)
        {
            try
            {
                var Matching = RequestBuyDao.Instance.GetDataMatching(id);
                ViewBag.Matching = Matching.ToList();
                ViewBag.MatchingCnt = Matching.Count();
                return Json(new returnsave { err = "0", errmsg = "OK" });

            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "1", errmsg = "Error" });
            }

        }
        public ActionResult matchdata(int id, int id_sale, int amt_sale)
        {
            try
            {
                //Session.Remove("member_id");
                string result = RequestBuyDao.Instance.ChkData(id, id_sale, amt_sale);
                if (result == "0")
                {
                    return Json(new returnsave { err = "1", errmsg = "사용 가능한 아이디 입니다.\n회원가입을 진행해 주세요." });
                }
                else if (result == "1")
                {

                    return Json(new returnsave { err = "0", errmsg = "사용중인 아이디 입니다. 다른 아이디를 입력해 주세요." });
                }
                else if (result == "2")
                {
                    return Json(new returnsave { err = "0", errmsg = "사용중인 아이디 입니다. 다른 아이디를 입력해 주세요." });
                }
                else
                {
                    return Json(new returnsave { err = "0", errmsg = "사용중인 아이디 입니다. 다른 아이디를 입력해 주세요." });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "0", errmsg = "Error" });
            }
        }

    }
}